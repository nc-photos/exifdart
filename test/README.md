# Test

Test files are curated from the internet

File:
- 1.heic
	- HEIC without EXIF data
	- From: https://github.com/nokiatech/heif/tree/gh-pages/content/images
- 1-2.heic
	- 1.heic with EXIF data added by exiftool
- 2.heic
	- iPhone X JPEG photo converted to HEIC by macOS 10.13 Preview app
	- From: https://avi.alkalay.net/2018/08/heic-lossless-images.html
- 3.hif
	- Canon EOS-1D X Mark III
	- From: https://github.com/strukturag/libheif/issues/199
- 3-2.hif
	- 3.hif with EXIF removed by exiftool
		- exiftool removed the EXIF data but left behind the Exif item in iinf and iloc box
- 4.hif
	- Canon EOS-1D X Mark III. Rotated 90 deg
	- From: https://github.com/strukturag/libheif/issues/219
- 5.jpg
	- [File:Valley of flowers uttaranchal full view.JPG](https://commons.wikimedia.org/w/index.php?curid=2148384) by [Raghuram. A](https://en.wikipedia.org/wiki/User:Araghu) is licensed with CC BY-SA 3.0
