import 'dart:io';

import 'package:exifdart/src/blob_reader_io.dart';
import 'package:exifdart/src/exif_extractor.dart';
import 'package:exifdart/src/log_message_sink.dart';
import 'package:test/test.dart';

void main() {
  test("Test readExif (heic canon)", () {
    final file = File("test/3.hif");
    return readExif(FileReader(file), log: _Log())
        .then((exif) => expect(exif?["Make"], "Canon"));
  });

  test("Test readMetadata (heic canon)", () {
    final file = File("test/3.hif");
    return readMetadata(FileReader(file), log: _Log())
        .then((metadata) => expect(metadata.exif?["Make"], "Canon"));
  });

  test("Test readExif (no exif)", () {
    final file = File("test/1.heic");
    return readExif(FileReader(file), log: _Log())
        .then((exif) => expect(exif, null));
  });

  test("Test readMetadata (jpg canon)", () {
    final file = File("test/5.jpg");
    return readMetadata(FileReader(file), log: _Log())
        .then((metadata) => expect(metadata.exif?["Make"], "Canon"));
  });

  /// Samsung saves the meta box not at the beginning of the file, unlike Apple
  test("Test readExif (samsung)", () async {
    final file = File("test/6.heic");
    final exif = await readExif(FileReader(file), log: _Log());
    expect(exif!["Make"], "samsung");
  });
}

class _Log extends LogMessageSink {
  @override
  void log(Object? message, [List<Object>? additional]) {
    // Uncomment to show logs
    // print("$message");
  }
}
