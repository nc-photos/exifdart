import 'package:exifdart/src/blob_view.dart';
import 'package:exifdart/src/log_message_sink.dart';

class HeicMetadataFinderResult {
  HeicMetadataFinderResult({
    this.imageWidth,
    this.imageHeight,
    this.rotateAngleCcw,
    this.exifExtentOffset,
  });

  final int? imageWidth;
  final int? imageHeight;
  final int? rotateAngleCcw;
  final int? exifExtentOffset;
}

class HeicMetadataFinder {
  HeicMetadataFinder(this.debug);

  /// Find where the EXIF data is stored in [dataView], or null if no EXIF data
  /// exists
  Future<HeicMetadataFinderResult> call(BlobView dataView) async {
    final length = dataView.byteLength;
    if (length < 12 || (await dataView.getUint32(0x04) != 0x66747970) // ftyp
        ) {
      debug?.log("Missing ftyp header");
      throw FormatException("Not a valid HEIC");
    }
    final fourcc = await dataView.getUint32(0x08);
    if (fourcc != 0x68656963 && // heic
            fourcc != 0x68656978 && // heix
            fourcc != 0x68657663 && // hevc
            fourcc != 0x6865696D && // heim
            fourcc != 0x68656973 && // heis
            fourcc != 0x6865766D && // hevm
            fourcc != 0x68657673 && // hevs
            fourcc != 0x6D696631 // mif1
        ) {
      debug?.log("Invalid fourcc: $fourcc");
      throw FormatException("Not a valid HEIC");
    }

    final metaOffset = await _findBoxOfType(dataView, _typeMeta);
    if (metaOffset == null) {
      debug?.log("Missing meta box");
      throw FormatException("Not a valid HEIC");
    }
    debug?.log("meta box @0x${metaOffset.toRadixString(16)}");
    // meta
    if (await dataView.getUint32(metaOffset + 4) != 0x6D657461) {
      debug?.log("Missing meta box");
      throw FormatException("Not a valid HEIC");
    }

    final iinfOffset = await _findChildOfType(dataView, metaOffset, _typeIinf);
    if (iinfOffset == null) {
      debug?.log("Missing iinf box");
      throw FormatException("Not a valid HEIC");
    }
    debug?.log("iinf box @0x${iinfOffset.toRadixString(16)}");

    final ilocOffset = await _findChildOfType(dataView, metaOffset, _typeIloc);
    if (ilocOffset == null) {
      debug?.log("Missing iloc box");
      throw FormatException("Not a valid HEIC");
    }
    debug?.log("iloc box @0x${ilocOffset.toRadixString(16)}");

    int? exifExtentOffset;
    try {
      exifExtentOffset = await _getExifExtentOffset(dataView,
          metaOffset: metaOffset,
          iinfOffset: iinfOffset,
          ilocOffset: ilocOffset);
    } catch (e, stacktrace) {
      debug?.log("$e, $stacktrace");
    }
    List<int?>? sizeOrientation;
    try {
      sizeOrientation = await _getSizeOrientation(dataView,
          metaOffset: metaOffset,
          iinfOffset: iinfOffset,
          ilocOffset: ilocOffset);
    } catch (e, stacktrace) {
      debug?.log("$e, $stacktrace");
    }
    return HeicMetadataFinderResult(
      imageWidth: sizeOrientation?[0],
      imageHeight: sizeOrientation?[1],
      rotateAngleCcw: sizeOrientation?[2],
      exifExtentOffset: exifExtentOffset,
    );
  }

  Future<int?> _getExifExtentOffset(
    BlobView dataView, {
    required int metaOffset,
    required int iinfOffset,
    required int ilocOffset,
  }) async {
    final exifInfeOffset =
        await _findInfeOfItemType(dataView, _typeExif, iinfOffset: iinfOffset);
    if (exifInfeOffset == null) {
      debug?.log("No EXIF infe entry");
      return null;
    }
    final exifItemId = await dataView.getUint16(exifInfeOffset + 12);
    debug?.log("EXIF item id: $exifItemId");

    final exifOffset = await _getExtentOffsetOfItemId(
      dataView,
      exifItemId,
      metaOffset: metaOffset,
      ilocOffset: ilocOffset,
    );
    if (exifOffset == null) {
      debug?.log("EXIF not found in iloc");
      throw FormatException("Not a valid HEIC");
    }
    debug?.log("EXIF data @0x${exifOffset.toRadixString(16)}");
    final exifHeader = await dataView.getUint32(exifOffset + 4);
    if (exifHeader != _typeExif) {
      debug?.log("EXIF data not found at the position pointed by iloc box: "
          "0x${exifOffset.toRadixString(16)}");
      throw FormatException("Not a valid HEIC");
    }
    return exifOffset;
  }

  /// Return the image size and orientation
  Future<List<int?>> _getSizeOrientation(
    BlobView dataView, {
    required int metaOffset,
    required int iinfOffset,
    required int ilocOffset,
  }) async {
    final pitmOffset = await _findChildOfType(dataView, metaOffset, _typePitm);
    if (pitmOffset == null) {
      throw FormatException("Not a valid HEIC: missing pitm box");
    }
    debug?.log("pitm box @0x${pitmOffset.toRadixString(16)}");
    final primaryItemId =
        await _getItemIdOfPitm(dataView, pitmOffset: pitmOffset);

    final iprpOffset = await _findChildOfType(dataView, metaOffset, _typeIprp);
    if (iprpOffset == null) {
      throw FormatException("Not a valid HEIC: missing iprp box");
    }
    final ipmaOffset = await _findChildOfType(dataView, iprpOffset, _typeIpma,
        isParentFullBox: false);
    if (ipmaOffset == null) {
      throw FormatException("Not a valid HEIC: missing ipma box");
    }
    debug?.log("ipma box @0x${ipmaOffset.toRadixString(16)}");
    final propertyIndexes = await _getIpmaAssociationsOfItemId(
        dataView, primaryItemId,
        ipmaOffset: ipmaOffset);
    if (propertyIndexes == null) {
      throw FormatException(
          "Not a valid HEIC: missing ipma entry for primary item 0x${primaryItemId.toRadixString(16)}");
    }
    debug?.log(
        "Property indexes for primary item: [${propertyIndexes.join(', ')}]");

    final ipcoOffset = await _findChildOfType(dataView, iprpOffset, _typeIpco,
        isParentFullBox: false);
    if (ipcoOffset == null) {
      throw FormatException("Not a valid HEIC: missing ipco box");
    }
    debug?.log("ipco box @0x${ipcoOffset.toRadixString(16)}");
    final propertyOffsets = await _findItemPropertyOfIndex(
        dataView, propertyIndexes,
        ipcoOffset: ipcoOffset);
    debug?.log("Property indexes for primary item: "
        "[${propertyOffsets.map((e) => e == null ? null : '0x' + e.toRadixString(16)).join(', ')}]");
    int? imageWidth, imageHeight, rotateAngleCcw;
    for (final propertyOffset in propertyOffsets) {
      if (propertyOffset == null) {
        // technically this is an invalid HEIC
        continue;
      }
      final propertyType = await dataView.getUint32(propertyOffset + 4);
      switch (propertyType) {
        case _typeIspe:
          imageWidth = await dataView.getUint32(propertyOffset + 12);
          imageHeight = await dataView.getUint32(propertyOffset + 16);
          break;

        case _typeIrot:
          rotateAngleCcw =
              ((await dataView.getUint8(propertyOffset + 8)) & 0x03) * 90;
          break;
      }
    }
    if (imageWidth == null || imageHeight == null) {
      throw FormatException("Not a valid HEIC: missing ispe box");
    }
    return [imageWidth, imageHeight, rotateAngleCcw];
  }

  /// Find the box of type [targetType] from the beginning of the file
  Future<int?> _findBoxOfType(BlobView dataView, int targetType) async {
    var offset = 0;
    while (offset < dataView.byteLength) {
      final size = await dataView.getUint32(offset);
      if (offset + size > dataView.byteLength) {
        debug?.log("Invalid size: $size (offset: $offset)");
        return null;
      }
      final type = await dataView.getUint32(offset + 4);
      if (type == targetType) {
        return offset;
      }
      offset += size;
    }
    return null;
  }

  /// Find the child box of type [targetType] in a parent box
  ///
  /// Assume parent is a FullBox by default, set [isParentFullBox] to false
  /// otherwise
  Future<int?> _findChildOfType(
    BlobView dataView,
    int parentOffset,
    int targetType, {
    bool isParentFullBox = true,
  }) async {
    final size = await dataView.getUint32(parentOffset);
    var offset = isParentFullBox ? 12 : 8;
    while (offset < size) {
      final childSize = await dataView.getUint32(parentOffset + offset);
      final childType = await dataView.getUint32(parentOffset + offset + 4);
      if (childType == targetType) {
        return parentOffset + offset;
      } else {
        offset += childSize;
      }
    }
    return null;
  }

  /// Find the infe box of itemType [targetType]
  Future<int?> _findInfeOfItemType(
    BlobView dataView,
    int targetType, {
    required int iinfOffset,
  }) async {
    var offset = 0;
    offset += 8;
    final version = await dataView.getUint8(iinfOffset + offset);
    final entryCount = await (version == 0
        ? dataView.getUint16(iinfOffset + 12)
        : dataView.getUint32(iinfOffset + 12));
    offset += 4 + (version == 0 ? 2 : 4);
    for (int i = 0; i < entryCount; ++i) {
      if (await _isInfeOfItemType(dataView, targetType,
          infeOffset: iinfOffset + offset)) {
        return iinfOffset + offset;
      }
      offset += await dataView.getUint32(iinfOffset + offset);
    }
    return null;
  }

  /// Return whether the infe box at [infeOffset] contains the itemType
  /// [targetType]
  Future<bool> _isInfeOfItemType(
    BlobView dataView,
    int targetType, {
    required int infeOffset,
  }) async {
    var offset = 0;
    offset += 4;
    final type = await dataView.getUint32(infeOffset + offset);
    if (type != _typeInfe) {
      throw FormatException("Not a valid HEIC");
    }
    offset += 4;
    final version = await dataView.getUint8(infeOffset + offset);
    offset += 4;

    if (version < 2) {
      // unsupported
      throw FormatException("HEIC not supported: infe box version < 2");
    }
    offset += version == 2 ? 4 : 6;
    final itemType = await dataView.getUint32(infeOffset + offset);
    return itemType == targetType;
  }

  /// Return the extent offset of itemID [targetItemId]
  Future<int?> _getExtentOffsetOfItemId(
    BlobView dataView,
    int targetItemId, {
    required int metaOffset,
    required int ilocOffset,
  }) async {
    final version = await dataView.getUint8(ilocOffset + 0x08);
    var offset = 0x0C;
    var temp = await dataView.getUint8(ilocOffset + offset);
    final offsetSize = (temp & 0xF0) >> 4;
    final lengthSize = temp & 0x0F;
    offset += 1;
    temp = await dataView.getUint8(ilocOffset + offset);
    final baseOffsetSize = (temp & 0xF0) >> 4;
    final indexSize = (version >= 1 ? temp & 0x0F : 0);
    offset += 1;
    final itemCount = await (version < 2
        ? dataView.getUint16(ilocOffset + offset)
        : dataView.getUint32(ilocOffset + offset));
    debug?.log("iloc offsetSize=$offsetSize, "
        "lengthSize=$lengthSize, "
        "baseOffsetSize=$baseOffsetSize, "
        "indexSize=$indexSize, "
        "itemCount=$itemCount");
    offset += version < 2 ? 2 : 4;
    for (int i = 0; i < itemCount; ++i) {
      final itemId = await (version < 2
          ? dataView.getUint16(ilocOffset + offset)
          : dataView.getUint32(ilocOffset + offset));
      offset += (version < 2 ? 2 : 4);
      final constructionMethod;
      if (version >= 1) {
        final temp = await dataView.getUint8(ilocOffset + offset + 1);
        constructionMethod = temp & 0x0F;
        offset += 2;
      } else {
        constructionMethod = 0;
      }
      offset += 2;
      final baseOffset = await _getDynamicSizeInt(
          dataView, ilocOffset + offset, baseOffsetSize);
      offset += baseOffsetSize;
      final extentCount = await dataView.getUint16(ilocOffset + offset);
      offset += 2;
      // debug?.log("itemId=$itemId, "
      //     "constructionMethod=$constructionMethod, "
      //     "baseOffset=$baseOffset, "
      //     "extentCount=$extentCount");
      if (itemId != targetItemId) {
        offset += (offsetSize + lengthSize + indexSize) * extentCount;
        continue;
      }
      if (constructionMethod == 2) {
        throw FormatException("Construction method == 2 is not supported");
      }
      if (extentCount != 1) {
        throw FormatException("Extent count > 1 is not supported");
      }
      final extentOffset =
          await _getDynamicSizeInt(dataView, ilocOffset + offset, offsetSize);
      debug?.log("extentOffset=${extentOffset.toRadixString(16)}");
      if (constructionMethod == 0) {
        return baseOffset + extentOffset;
      } else if (constructionMethod == 1) {
        final idatOffset =
            await _findChildOfType(dataView, metaOffset, _typeIdat);
        if (idatOffset == null) {
          throw FormatException("idat referenced but not found");
        }
        debug?.log("idat box @0x${idatOffset.toRadixString(16)}");
        return idatOffset + 8 + baseOffset + extentOffset;
      }
    }
    return null;
  }

  /// Return the item ID in a pitm box
  Future<int> _getItemIdOfPitm(
    BlobView dataView, {
    required int pitmOffset,
  }) async {
    final version = await dataView.getUint8(pitmOffset + 8);
    if (version == 0) {
      return dataView.getUint16(pitmOffset + 12);
    } else {
      return dataView.getUint32(pitmOffset + 12);
    }
  }

  /// Return the associated property indexes of item ID [targetItemId] in a ipma
  /// box
  Future<List<int>?> _getIpmaAssociationsOfItemId(
    BlobView dataView,
    int targetItemId, {
    required int ipmaOffset,
  }) async {
    var offset = 0;
    offset += 8;
    final version = await dataView.getUint8(ipmaOffset + offset);
    final flags = (await dataView.getUint32(ipmaOffset + offset)) & 0xFFFFFF;
    offset += 4;
    final entryCount = await dataView.getUint32(ipmaOffset + offset);
    offset += 4;
    for (var i = 0; i < entryCount; ++i) {
      final int itemId;
      if (version < 1) {
        itemId = await dataView.getUint16(ipmaOffset + offset);
        offset += 2;
      } else {
        itemId = await dataView.getUint32(ipmaOffset + offset);
        offset += 4;
      }
      final associationCount = await dataView.getUint8(ipmaOffset + offset);
      offset += 1;
      final product = List<int>.filled(associationCount, 0);
      for (var j = 0; j < associationCount; ++j) {
        final int propertyIndex;
        if ((flags & 1) != 0) {
          propertyIndex =
              (await dataView.getUint16(ipmaOffset + offset)) & 0x7FFF;
          offset += 2;
        } else {
          propertyIndex = (await dataView.getUint8(ipmaOffset + offset)) & 0x7F;
          offset += 1;
        }
        product[j] = propertyIndex;
      }

      if (itemId == targetItemId) {
        return product;
      }
    }
    return null;
  }

  /// Find the item properties of indexes [targetIndexes] in a ipco box
  Future<List<int?>> _findItemPropertyOfIndex(
    BlobView dataView,
    List<int> targetIndexes, {
    required int ipcoOffset,
  }) async {
    final size = await dataView.getUint32(ipcoOffset);
    var offset = 0;
    offset += 8;
    final product = List<int?>.filled(targetIndexes.length, null);
    var i = 1;
    while (offset < size) {
      final found = targetIndexes.indexOf(i);
      if (found != -1) {
        product[found] = ipcoOffset + offset;
      }
      final childSize = await dataView.getUint32(ipcoOffset + offset);
      offset += childSize;
      i += 1;
    }
    return product;
  }

  Future<int> _getDynamicSizeInt(
      BlobView dataView, int offset, int byteSize) async {
    switch (byteSize) {
      case 0:
        return 0;
      case 4:
        return dataView.getUint32(offset);
      case 8:
        return dataView.getUint64(offset);
      default:
        throw FormatException("Not a valid HEIC");
    }
  }

  final LogMessageSink? debug;

  static const _typeMeta = 0x6D657461; // meta
  static const _typeExif = 0x45786966; // Exif
  static const _typeIdat = 0x69646174; // idat
  static const _typeIinf = 0x69696E66; // iinf
  static const _typeIloc = 0x696C6F63; // iloc
  static const _typeInfe = 0x696E6665; // infe
  static const _typeIpco = 0x6970636F; // ipco
  static const _typeIpma = 0x69706D61; // ipma
  static const _typeIprp = 0x69707270; // iprp
  static const _typeIrot = 0x69726F74; // irot
  static const _typeIspe = 0x69737065; // ispe
  static const _typePitm = 0x7069746D; // pitm
}
